package edu.sjsu.crypto.ciphersys.hash;

import edu.sjsu.yazdankhah.crypto.util.abstracts.Md4Abs;
import edu.sjsu.yazdankhah.crypto.util.primitivedatatypes.Word;
import edu.sjsu.yazdankhah.crypto.util.cipherutils.ConversionUtil;
import edu.sjsu.yazdankhah.crypto.util.cipherutils.GeneralUtil;


/**
 * @author Inhee Park
 * 
 *         CS265 - Final - MD4 Hashing System
 * 
 */
public class Md4Sys extends Md4Abs {
	// attribute
	private Word A, B, C, D;
	
	// constructor
	public Md4Sys() {}

	/////////////////////////////////////////////////
	public String MD4(String message) {
		/*
		 * Step #1: convert message into binStr (Mb + B) 
		 * private String Step1(String message) { 
		 * return md4Padding(binStr);
		 */
		String msgBinStr = Step1(message);

		/*
		 * Step #2: convert original message length to binStr (L) 
		 * private String Step2(int msgOrigSize) 
		 * return md4SizeToBinStrLittleEndian( msgOrigSize );
		 */
		String sizeBinStr = Step2(ConversionUtil.textToBinStr(message).length());

		/*
		 * Step #3: construct Words array M (byte-level little-endian) 
		 * private Word[] Step3(String msgBinStr, String sizeBinStr) 
		 * return M;
		 */
		Word[] M = Step3(msgBinStr, sizeBinStr);
		
		/*
		 * Step #4: convert M into matrix X[][] 
		 * private Word[][] Step4(Word[] M) 
		 * return X;
		 */
		Word[][] X = Step4(M);

		/*
		 * Step #5: initialize buffers A, B, C, D
		 */
		Step5();

		/*
		 * Step #6: hash blocks 
		 * private void Step6(Word[][] X)
		 */
		Step6(X);

		/*
		 * Step #7: return message digest 
		 * private String Step7() 
		 * return ConversionUtil.wordArrToHexStr(ABCD);
		 */
		String MD4hexStr = Step7();
		return MD4hexStr;
	}
	/////////////////////////////////////////////////
	 

	// Step #1: convert message into binStr (Mb + B)
	private String Step1(String message) {
		String binStr = ConversionUtil.textToBinStr(message);
		return md4Padding(binStr);
	}

	// Step #2: convert original message length to binStr (L)
	private String Step2(int msgOrigSize) {
	    return md4SizeToBinStrLittleEndian(msgOrigSize);
	}

	// Step #3: construct Words array M (byte-level little-endian)
	private Word[] Step3(String msgBinStr, String sizeBinStr) {
		Word[] msgArr = ConversionUtil.binStrToWordArrLittleEndian(msgBinStr);
		Word[] sizeArr = ConversionUtil.binStrToWordArr(sizeBinStr);
		Word[] M = GeneralUtil.appendWordArrs(msgArr, sizeArr);
		return M;
	}

	// Step #4: convert M into matrix X[][]
	private Word[][] Step4(Word[] M) {
		Word[][] X = ConversionUtil.wordArrToWordMatrix(M, MD4_BLOCK_SIZE_WORDS);
		return X;
	}

	// Step #5: initialize buffers A, B, C, D
	private void Step5() {
		A = A_INIT.clone();
		B = B_INIT.clone();
		C = C_INIT.clone();
		D = D_INIT.clone();
	}

	// Step #6: hash blocks
	private void Step6(Word[][] X) {
		for (int i = 0; i < X.length; i++) 
			HashOneBlock(A, B, C, D, X[i]);
	}

	// Step #6a: hash one block
	private void HashOneBlock(Word A, Word B, Word C, Word D, Word[] Xi) {
		Word AA = A.clone();
		Word BB = B.clone();
		Word CC = C.clone();
		Word DD = D.clone();
		
		//////// CRUCIAL to use r++ not ++r/////////////
		for (int r = 0; r <= 2; r++) 
			Round(r, A, B, C, D, Xi);

		A.addMod2p32M(AA);
		B.addMod2p32M(BB);
		C.addMod2p32M(CC);
		D.addMod2p32M(DD);
	}

	// Step #6b: round method
	private void Round(int r, Word Ai, Word Bi, Word Ci, Word Di, Word[] Xi) {
		for (int j = 0; j <= 15; j++) {
			Word Ai_1 = Ai.clone();
			Word Bi_1 = Bi.clone();
			Word Ci_1 = Ci.clone();
			Word Di_1 = Di.clone();
			//////// CRUCIAL to use this.setM()/////////////
			Ai.setM(Di_1);
			Ci.setM(Bi_1);
			Di.setM(Ci_1);
			Word tmp = Ai_1.addMod2p32( func(r, Bi_1, Ci_1, Di_1) );
			tmp.addMod2p32M( Xi[PERMUTATION[r][j]] );
			tmp.addMod2p32M( K[r] );
			tmp.rotateLeftM( ROTATE[r][j] );
			Bi.setM(tmp);
		}
	}


	// Step #7: return message digest
	private String Step7() {
		Word[] ABCD_LE = new Word[] { 
				A.toLittleEndianFormatM(), 
				B.toLittleEndianFormatM(), 
				C.toLittleEndianFormatM(), 
				D.toLittleEndianFormatM()};
		return ConversionUtil.wordArrToHexStr(ABCD_LE);
	}
}
