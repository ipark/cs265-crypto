package edu.sjsu.crypto.ciphersys.hash;

import org.junit.jupiter.api.Test;
import lombok.extern.slf4j.Slf4j;

/**
 * @author Inhee Park
 * 
 *         CS265 - Final - MD4 Hashing System Test
 * 
 */
@Slf4j
class Md4Test {

	@Test
	void testHashDemo1() {
		Md4Sys sys = new Md4Sys();
		String message = "defend the east wall!";
		String testVector = "ef6cbe2be40d16b3f1b63ac32aad97e8";
		log.info("\nMD4(" + message + ") = [" + sys.MD4(message) + "]");
		System.out.println(sys.MD4(message).equals(testVector));
	}
	@Test
	void testHashDemo2() {	
		Md4Sys sys = new Md4Sys();
		String message = "";
		String testVector = "31d6cfe0d16ae931b73c59d7e0c089c0";
		log.info("\nMD4(" + message + ") = [" + sys.MD4(message) + "]");
		System.out.println(sys.MD4(message).equals(testVector));
	}
	@Test
	void testHashDemo3() {
		Md4Sys sys = new Md4Sys();
		String message = "a";
		String testVector = "bde52cb31de33e46245e05fbdbd6fb24";
		log.info("\nMD4(" + message + ") = [" + sys.MD4(message) + "]");
		System.out.println(sys.MD4(message).equals(testVector));
	}
	@Test
	void testHashDemo4() {	
		Md4Sys sys = new Md4Sys();
		String message = "abc";
		String testVector = "a448017aaf21d8525fc10ae87aa6729d";
		log.info("\nMD4(" + message + ") = [" + sys.MD4(message) + "]");
		System.out.println(sys.MD4(message).equals(testVector));
	}
	@Test
	void testHashDemo5() {	
		Md4Sys sys = new Md4Sys();
		String message = "message digest";
		String testVector = "d9130a8164549fe818874806e1c7014b";
		log.info("\nMD4(" + message + ") = [" + sys.MD4(message) + "]");
		System.out.println(sys.MD4(message).equals(testVector));
	}
	@Test
	void testHashDemo6() {	
		Md4Sys sys = new Md4Sys();
		String message = "abcdefghijklmnopqrstuvwxyz";
		String testVector = "d79e1c308aa5bbcdeea8ed63df412da9";
		log.info("\nMD4(" + message + ") = [" + sys.MD4(message) + "]");
		System.out.println(sys.MD4(message).equals(testVector));
	}

}
