package edu.sjsu.crypto.ciphersys.classic;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class SimpleSubSysTest {

	@Test
	void testEncryptionDemo() {
		SimpleSubSys.encryptionDemo();
	}
	
	@Test
	void testDecryptionDemo() {
		SimpleSubSys.decryptionDemo();
	}
}

