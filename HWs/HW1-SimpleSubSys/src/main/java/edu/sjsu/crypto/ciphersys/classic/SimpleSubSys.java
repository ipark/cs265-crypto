package edu.sjsu.crypto.ciphersys.classic;

import java.util.Map;

import edu.sjsu.yazdankhah.crypto.util.abstracts.SimpleSubAbs;
import edu.sjsu.yazdankhah.crypto.util.shiftregisters.CSR;
import lombok.extern.slf4j.Slf4j;

/**
 * @author Inhee Park
 * 
 * CS265 Programming Assignment #1
 * SimpleSubSys Class for Simple Substitution Cipher System
 * 
 * My customer name is Tracy Ho
 * ciphertext = "RGGCZVU TIPGKRERCPJZJ: SIVRBZEX TZGYVIJ ZE KYV IVRC NFICU"
 * key = 17
 * 
 * plaintext = "applied cryptanalysis: breaking ciphers in the real world"
 * 
 */

@Slf4j
public class SimpleSubSys extends SimpleSubAbs{
	private int key; // key used for encryption and decryption
	String plaintext; // input plain English text
	String ciphertext; // encrypted text
	private CSR plainCode; // 26 plain English Letters
	private CSR encryptionCode; // Letters after rotate LEFT by key amount 
	private CSR decryptionCode; // Letters after rotate RIGHT by key amount
	
	// encryptionTable hashMap whose (k, v) pair denotes
	// k: plain English Letter
	// v: encrypted by rotationLeftM
	private static Map<Character, Character> encryptionTable;

	// decyptionTable hashMap whose (k, v) pair denotes
	// k: encrypted letter
	// v: decrypted letter by rotationRightM
	private static Map<Character, Character> decryptionTable;
	
	// constructor
	public SimpleSubSys(int myKey) {
		key = myKey;
		// 26 plain English Letter copied to CSR type
		plainCode = CSR.constructFromText(ENGLISH_ALPHABET_STR);
		// encryption code is generated based on the plain English letters
		// via rotate Left by key amount
		encryptionCode = plainCode.clone().rotateLeftM(myKey);
		// then make lookup table (k, v) pair
		// k: plainCode
		// v: encryptionCode
		encryptionTable = makeLookupTable(plainCode, encryptionCode);
		//
		// decryption code is generated based on the encryptionCode
		// via rotate Right by key amount
		decryptionCode = encryptionCode.clone().rotateRightM(myKey);
		// then make lookup table (k, v) pair
		// k: encryptionCode
		// v: decryptionCode
		decryptionTable = makeLookupTable(encryptionCode, decryptionCode);
	}
	/*
	 * method for encryption 
	 * @input String plaintext
	 * @return String ciphertext
	 */
	public String encrypt(String plaintext) {
		// always first make character lowercase
		plaintext = plaintext.toLowerCase();
		
		//log.info(plaintext);
		
		String encryptedStr = "";
		
		// repeat for every letter from input plaintext
		for (char c: plaintext.toCharArray()) {
			// if current character is not English letter
			// just print it as it is
			if (!Character.isAlphabetic(c)) {
				encryptedStr += c;
			}
			// if current character is English letter
			// then concatenate the corresponding encryption code
			// from encryption lookup table
			else {
				encryptedStr += encryptionTable.get(c);
			}
		}
		// finally make ciphertext uppercase
		// by following convention 
		ciphertext = encryptedStr.toUpperCase();
		return ciphertext;
	}
	
	/*
	 * method for decryption 
	 * @input String ciphertext
	 * @return String plaintext (recovered)
	 */
	public String decrypt(String ciphertext) {
		// always first make character lowercase
		ciphertext = ciphertext.toLowerCase();
		
		String decryptedStr = "";
		
		// repeat for every letter from input plaintext
		for (char c: ciphertext.toCharArray()) {
			// if current character is not English letter
			// just print it as it is
			if (!Character.isAlphabetic(c)) {
				decryptedStr += c;
			}
			// if current character is English letter
			// then concatenate the corresponding decryption code
			// from decryption lookup table
			else {
				decryptedStr += decryptionTable.get(c);
			}
		}
		// finally make recovered plaintext lowercase
		// by following convention 
		plaintext = decryptedStr.toLowerCase();
		return plaintext;
	}
	
	/*
	 * demonstrate encryption by JUnitTest 
	 */
	public static void encryptionDemo() {
		// given test case
		String plaintext = "defend the east wall of the castle";
		int key = 5; 
	
		// my own test case
		// resulting ciphertext with key value should be provided
		// to my customer
		//String plaintext = "applied cryptanalysis: breaking ciphers in the real world";
		//int key = 17;

		SimpleSubSys sys = new SimpleSubSys(key);
		log.info("Encryption Demo");
		log.info("INPUT:");
		log.info("     plaintext = " + plaintext);
		log.info("     key = " + key);
		log.info("OUTPUT:");
		log.info("     Ciphertext = [" + sys.encrypt(plaintext)+ "]\n");
	}
	
	/*
	 * demonstrate decryption by JUnitTest 
	 */
	public static void decryptionDemo() {
		// given test case
		String ciphertext = "IJKJSI YMJ JFXY BFQQ TK YMJ HFXYQJ";
		int key = 5;
		
		// my own test case
		// ciphertext with key value should be provided to my customer
		//String ciphertext = "RGGCZVU TIPGKRERCPJZJ: SIVRBZEX TZGYVIJ ZE KYV IVRC NFICU";
		//int key = 17;

		SimpleSubSys sys = new SimpleSubSys(key);
		log.info("Decryption Demo");
		log.info("INPUT:");
		log.info("       ciphertext = " + ciphertext);
		log.info("       key = " + key);
		log.info("OUTPUT:");
		log.info("       Recovered Plaintext = [" + sys.decrypt(ciphertext)+ "]");
	}

}
