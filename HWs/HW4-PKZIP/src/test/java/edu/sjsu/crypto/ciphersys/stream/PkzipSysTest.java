package edu.sjsu.crypto.ciphersys.stream;


import org.junit.jupiter.api.Test;
import lombok.extern.slf4j.Slf4j;

@Slf4j
class PkzipSysTest {

	@Test
	void testEncryptionDemo() {
//		String plaintext = "defend the east wall!"; 
//		String pass = "GoToHell#007";
		
		String plaintext = "%%% adversarial machine learning %%%"; 
		String pass = "arXiv:2002.05648v17";
						 
		PkzipSys sys = new PkzipSys (pass);
		log.info("ciphertext = [" + sys.encrypt(plaintext)+ "]");
	}
	
	@Test
	void testDecryptionDemo() {
//		String ciphertext = "1f63ddbf86138a442cd1d6ba4a91a8596bca1090a2"; 
//		String pass = "GoToHell#007";
		
		String ciphertext = "0a721555e0938e05b40ab79797ccf9ea17083388aa6ebb4f0f0c7b106ccd2e427af13292";
		String pass = "arXiv:2002.05648v17";
		
		PkzipSys sysR = new PkzipSys (pass);
		log.info("Recovered Plaintext = [" + sysR.decrypt(ciphertext)+ "]");
	}
}

