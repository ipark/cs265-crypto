package edu.sjsu.crypto.ciphersys.stream;

import edu.sjsu.yazdankhah.crypto.util.abstracts.PkzipAbs;
import edu.sjsu.yazdankhah.crypto.util.cipherutils.ConversionUtil;
import edu.sjsu.yazdankhah.crypto.util.cipherutils.StringUtil;
import edu.sjsu.yazdankhah.crypto.util.primitivedatatypes.UByte;
import edu.sjsu.yazdankhah.crypto.util.primitivedatatypes.Word;


/**
 * @author Inhee Park
 * 
 * CS265 Programming Assignment #4 : PKZIP
 * 
 * My customers are <Tracy Ho> and <Rakesh Nagaraju>. 
 * 
 * ciphertext = [0a721555e0938e05b40ab79797ccf9ea17083388aa6ebb4f0f0c7b106ccd2e427af13292]
 * pass = "arXiv:2002.05648v1"
 * 
 * 
 * Recovered Plaintext = [%%% adversarial machine learning %%%]
 * 
 */


public class PkzipSys extends PkzipAbs {
	// attributes
	Word X, Y, Z; 
	
	// constructor
	public PkzipSys(String pass) {   	
		// pass: text -> binary string
		String keyBinStr = ConversionUtil.textToBinStr(pass);
		
		// extract 96-bit from binary string
		keyBinStr = StringUtil.rightTruncRightPadWithZeros(keyBinStr, KEY_SIZE_BITS); 
		
		// 32-bit binary string for X-register
		String xbin = keyBinStr.substring(0,  31+1); // 31-0+1=32
		// 32-bit binary string for Y-register
		String ybin = keyBinStr.substring(32, 63+1); // 63-32+1=32
		// 32-bit binary string for Z-register
		String zbin = keyBinStr.substring(64, 95+1); // 95-64+1=32 
		
		// convert binary string to 32-bit Word
		X = Word.constructFromBinStr(xbin);
		Y = Word.constructFromBinStr(ybin);
		Z = Word.constructFromBinStr(zbin);
    }
	
	/**
	 * generateKey(Z)
	 * Generates key stream one byte at a time based on the given word.
	 * 
	 * @param v - the given word object
	 * @return key - the key as an unsigned byte
	 * 
	 */
	public UByte generateKey(Word v) {
		Word t; // 32-bit
		UByte key; // 8-bit

		// t = < Z V 3 >16..31 
		t = v.or(THREE_WORD).rightHalfAsWord(); // higher 16-bit as word, 0 otherwise
		
		// k = < (t * (t xor 1)) >> 8 >24..31
		key = t.multiplyMod2p32(t.xor(Word.ONE_WORD)).shiftRight(KEY_GENERATION_SHIFTS).byteAt(3);

		return key;
	}
	
	/** 
	 * CRC (Cyclic Redundancy Check)
	 * Make some encryption on the given word based on its current content 
	 * and the value of the given unsigned byte.
	 * 
	 * @param vM - the given word
	 * @param b - the given unsigned byte
	 * 
	 */
	public void CRC(Word vM, UByte b) {
		Word Wb = Word.constructFromUByte(b);
		// V = V (xor) b
		vM.xorM(Wb);
		
		// CRUCIAL POINT: total 8 iterations
		// In Lecture Note, for i = 0 to 7
		// Pre-defined constant CRC_TERATION is 8
		for (int i = 0; i < CRC_ITERATION; ++i) {			
			// V = V >> 1
			vM.shiftRightM(1);			
			
			if (vM.toLong() % 2 != 0) {// V is odd
				// V = V (xor) 0xedb88320
				vM.xorM(CRC_CONST_WORD);
			}
		}
	}
	/**
	 * update(X, Y, Z, p)
	 * Updates the given words contents based on their current content 
	 * and the value of the given unsigned byte.
	 * 
	 * @param xM - the given word
	 * @param yM - the given word
	 * @param zM - the given word
	 * @param p - the given unsigned byte
	 * 
	 */
	public void update(Word xM, Word yM, Word zM, UByte p) {
		// X = CRC(X, P)
		CRC(xM, p); // return type of CRC is void, no return
		
		// Y = (Y + <X>24..31) * 134775813 + 1 (mode 2^32)
		Word Wx = Word.constructFromUByte(xM.byteAt(3)); // <X>24..31
		yM.addMod2p32M(Wx); //  yM + <X>24..31
		yM.multiplyMod2p32M(UPDATE_CONST_WORD); // yM * 134775813+1 
		yM.addMod2p32M(Word.ONE_WORD); // yM + 1
		
		// Z = CRC(Z, <Y>0..7)
		CRC(zM, yM.byteAt(0));
	}

	/** 
	 * encrypt
	 * 
	 * @param plaintext in String format
	 * @return ciphertext in HEX format
	 * 
	 */	
	public String encrypt(String plaintext) {
		// text -> binStr
		String pBinStr = ConversionUtil.textToBinStr(plaintext);
		// to ensure partition binStr into length of 8
		pBinStr = StringUtil.toDividableByNRightPadZero(pBinStr, 8);

		// binStr -> UByte array
		UByte[] pUbArr = ConversionUtil.binStrToUByteArr(pBinStr);

		// store encrypted UByte at UByte array 
		UByte[] cUbArr = new UByte[pUbArr.length];
		
		UByte Pi, Ki, Ci;
		// encrypt one UByte at a time from generateKey(Z)
		for (int i = 0; i < pUbArr.length; i++) {
			Pi = pUbArr[i];
			Ki = generateKey(Z);
			// Ci = Pi(xor)Ki
			Ci = Pi.xor(Ki); 
			cUbArr[i] = Ci;
			// update
			update(X, Y, Z, Pi);
		}
		// UByte array -> binStr
		String binStr = ConversionUtil.ubyteArrToBinStr(cUbArr);
		// binStr -> HEX format
		String ciphertext = ConversionUtil.binStrToHexStr(binStr);
		return ciphertext;
	}
	/** 
	 * decrypt
	 * 
	 * @param ciphertext in HEX format
	 * @return plaintext in String format
	 * 
	 */	
	public String decrypt(String ciphertext) {	
		// HEX format -> binStr
		String cBinStr = ConversionUtil.hexStrToBinStr(ciphertext);
		// to ensure partition binStr into length of 8
		cBinStr = StringUtil.toDividableByNRightPadZero(cBinStr, 8);

		
		// binStr -> UByte array
		UByte[] cUbArr = ConversionUtil.binStrToUByteArr(cBinStr);;
		
		// store decrypted UByte at UByte array
		UByte[] pUbArr = new UByte[cUbArr.length];
		
		UByte Pi, Ki, Ci;
		// decrypt one UByte at a time from generateKey(Z)
		for (int i = 0; i < cUbArr.length; i++) {
			Ci = cUbArr[i];
			Ki = generateKey(Z);
			Pi = Ci.xor(Ki); // Pi = Ci(xor)Ki
			pUbArr[i] = Pi;
			// update	
			update(X, Y, Z, Pi);
		}
		// UByte array ->  String format
		String plaintext = ConversionUtil.ubyteArrToText(pUbArr);
		return plaintext;
	}
}
	
	
