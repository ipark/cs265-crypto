package edu.sjsu.crypto.ciphersys.classic;


import edu.sjsu.yazdankhah.crypto.util.abstracts.DoubleTransAbs;
import edu.sjsu.yazdankhah.crypto.util.cipherutils.ConversionUtil;
import edu.sjsu.yazdankhah.crypto.util.matrixdatatypes.CharMatrix;

/**
 * @author Inhee Park
 * 
 * CS265 Programming Assignment #2
 * DoubleTransSys Class for Double Transposition
 * 
 * My customer name is Tracy Ho
 * ciphertext = [OWRATTMURPE ONIR,UFO BY TH MNUOLCN EPE ONIATTMURBY    EERTH ]
 * int[] rowsPerm = {0, 3, 2, 1};
 * int[] colsPerm = {2, 0, 1}; 
 * 
 * plaintext = [row permutation by four, then column permutation by three]
 * 
 */

public class DoubleTransSys extends DoubleTransAbs {
	private CharMatrix[] encryptMat; 
	private CharMatrix[] decryptMat;
	int[] rowsPermutation, colsPermutation; 
	int rows, cols;
	
	// constructor
    public DoubleTransSys(int[] rowsPerm, int[] colsPerm) {
    	// attributes 
    	rowsPermutation = rowsPerm.clone(); // row permutation index list
    	colsPermutation = colsPerm.clone(); // col permutation index list
    	rows = rowsPerm.length; 
    	cols = colsPerm.length;	
    }

    // encryption by double transposition (rowPermute->colPermute)
	public String encrypt(String plaintext) {
		
		StringBuilder sb = new StringBuilder("");
		// convert plaintext to rows x cols CharMatrix
		encryptMat = ConversionUtil.textToCharMatrixArr(rows, cols, plaintext);
		
		// encryptMat is an array whose element is CharMatrix
		// thus apply double transposition to each element encryptMat[i]
		for (int i = 0; i < encryptMat.length; i++) {
			
			encryptMat[i].permuteRowsM(rowsPermutation);
			encryptMat[i].permuteColsM(colsPermutation);
			
			sb.append(encryptMat[i].toText().toUpperCase());
		}
		String ciphertext = sb.toString();
		return ciphertext;
	}
	
    // decryption by double transposition (colPermute->rowPermute)
	public String decrypt(String ciphertext) {
		
		StringBuilder sb = new StringBuilder("");
		// convert ciphertext to rows x cols CharMatrix
		decryptMat = ConversionUtil.textToCharMatrixArr(rows, cols, ciphertext);
		
		// decryptMat is an array whose element is CharMatrix
		// thus apply reverse double transposition to each element encryptMat[i]
		for (int i = 0; i < decryptMat.length; i++) {

			decryptMat[i].inversePermuteColsM(colsPermutation);
			decryptMat[i].inversePermuteRowsM(rowsPermutation);

			sb.append(decryptMat[i].toText().toLowerCase());  
		}
		String plaintext = sb.toString().trim(); // trim trailing white spaces
		return plaintext;
	}
}
	
	
