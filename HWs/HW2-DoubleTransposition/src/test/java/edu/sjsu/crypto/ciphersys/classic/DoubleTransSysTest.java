package edu.sjsu.crypto.ciphersys.classic;


import org.junit.jupiter.api.Test;
import lombok.extern.slf4j.Slf4j;

@Slf4j
class DoubleTransSysTest {

	@Test
	void testEncryptionDemo() {
/*
		String plaintext = "defend the east wall"; 
//                          012345678901234567890
		int[] rowsPerm = { 2, 0, 1 };
		int[] colsPerm = { 3, 2, 0, 1 };
*/

		String plaintext = "row permutation by four, then column permutation by three";
		int[] rowsPerm = {0, 3, 2, 1};
		int[] colsPerm = {2, 0, 1}; 

		DoubleTransSys sys = new DoubleTransSys(rowsPerm, colsPerm); 
		log.info("ciphertext = [" + sys.encrypt(plaintext)+ "]\n");
	}
	
	@Test
	void testDecryptionDemo() {
/*
  		String ciphertext = " EEHFEED TDN    T SALLAW";
//                           012345678901234567890123
		int[] rowsPerm = { 2, 0, 1 };
		int[] colsPerm = { 3, 2, 0, 1 };
*/

 		String ciphertext = "OWRATTMURPE ONIR,UFO BY TH MNUOLCN EPE ONIATTMURBY    EERTH ";
		int[] rowsPerm = {0, 3, 2, 1};
		int[] colsPerm = {2, 0, 1}; 

/*
		String ciphertext = " 2YMISHTS I UBOD HDN, W/ SYTRELSSNA";
		int[] rowsPerm = { 2, 0, 1 };
		int[] colsPerm = { 3, 2, 0, 1 };
*/
 
		DoubleTransSys sys = new DoubleTransSys(rowsPerm, colsPerm);
		log.info("Recovered Plaintext = [" + sys.decrypt(ciphertext)+ "]\n");
	}
	
}

