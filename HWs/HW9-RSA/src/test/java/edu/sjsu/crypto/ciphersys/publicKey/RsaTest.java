package edu.sjsu.crypto.ciphersys.publicKey;

import org.junit.jupiter.api.Test;
import lombok.extern.slf4j.Slf4j;
import java.io.File;

@Slf4j
class RsaTest {

	String path = new File(".").getAbsolutePath();
/*
	@Test
	void testGenerateKeyDemo() {
		String pass = "Diffie-Hellman:Trudy's Dilemma";
		String keyHolderName = "InheePark";

		String publicKeyFile = path + "/src/test/resources/IPrsaPublicKey.txt";
		String privateKeyFile = path + "/src/test/resources/IPrsaPrivateKey.txt";

		RsaSys sys = new RsaSys();
		sys.generateKeys(pass, keyHolderName);
		sys.getPrivateKey().save(privateKeyFile);
		sys.getPublicKey().save(publicKeyFile);

		log.info("Private Key = [\n" + sys.getPrivateKey() + "\n]\n");
		log.info("Public Key = [\n" + sys.getPublicKey() + "\n]\n");
	}
*/
	@Test
	void testEncryptionDemo() {
//  	String plaintext = "defend the east wall!";
// 		String publicKeyFile = path + "/src/test/resources/rsaPublicKey.txt";

		String plaintext = "Virtual Robot Locomotion With Adversarial Reinforcement Learning";
		// String publicKeyFile = path + "/src/test/resources/IPrsaPublicKey.txt";		
		String publicKeyFile = path + "/src/test/resources/Tracy_RSAPublicKey.txt";

 		RsaSys sys = new RsaSys();
		sys.getPublicKey().restore(publicKeyFile);
		log.info("ciphertext = [" + sys.encrypt(plaintext, sys.getPublicKey()) + "]");
	}

	@Test
	void testDecryptionDemo() {
//  	String ciphertext = "17d3007684940b17f0709c8139ea2944764ed5d76c1995213d19eb4bc53342976532b59066c147eafb9 7fc880b3a86ad";
//  	String privateKeyFile = path + "/src/test/resources/rsaPrivateKey.txt";

		String ciphertext = "0000000000000000499b6d2e3a5814a6000000000000000066441178b8db7dff000000000000000022f37ff41a49f5ce00000000000000001c95a0f7506c3421000000000000000068070634eb806fc100000000000000007630a1cde1ec2fe500000000000000001889d3a45e48bb2700000000000000005cd693ff8297ce5e";
    	String privateKeyFile = path + "/src/test/resources/IPrsaPrivateKey.txt";

		RsaSys sys = new RsaSys();
		sys.getPrivateKey().restore(privateKeyFile);
		log.info("PlaintextR = [" + sys.decrypt(ciphertext, sys.getPrivateKey()) + "]");
	}
}
