package edu.sjsu.crypto.ciphersys.publicKey;

import edu.sjsu.yazdankhah.crypto.util.abstracts.RsaAbs;
import edu.sjsu.yazdankhah.crypto.util.ciphersysdatatypes.RsaPrivateKey;
import edu.sjsu.yazdankhah.crypto.util.ciphersysdatatypes.RsaPublicKey;
import edu.sjsu.yazdankhah.crypto.util.cipherutils.ConversionUtil;
import edu.sjsu.yazdankhah.crypto.util.cipherutils.Function;
import edu.sjsu.yazdankhah.crypto.util.cipherutils.StringUtil;
import lombok.Data;
import java.math.BigInteger;
import java.util.Random;

/**
 * @author Inhee Park
 * 
 *         CS265 - HW9 - RSA Cipher System
 * 
 *         My customers are Rakesh and Tracy
 * 
 *         pass=[Diffie-Hellman:Trudy's Dilemma] 
 *         plaintext=[Virtual Robot Locomotion With Adversarial Reinforcement Learning]
 *
 */
@Data
public class RsaSys extends RsaAbs {
	// attributes
	private RsaPublicKey publicKey;
	private RsaPrivateKey privateKey;

	// constructor
	public RsaSys() {
		publicKey = new RsaPublicKey(null, null, null);
		privateKey = new RsaPrivateKey(null, null);
	}

	/**
	 * generateKeys - generate and save public-key and private-key files using
	 * random generator64 using pass, based on that random number generate a prime
	 * number e (encryption exponent), followed by generating two prime numbers (p
	 * and q) bigger than e.
	 * 
	 * @param pass
	 * @param keyHolderName
	 * 
	 */
	public void generateKeys(String pass, String keyHolderName) {
		pass = StringUtil.rightTruncRightPadWithZeros(pass, KEY_SIZE_BITS); // 64

		// choose large two primes p and q bigger than
		// encryption exponent, e
		Random rnd = Function.getRandomGenerator64(pass);
		BigInteger e = Function.generateRandomPrimeBigInteger(E_SIZE_BITS, rnd);
		BigInteger p = Function.generateRandomPrimeBigIntegerBiggerThan(e, rnd);
		BigInteger q = Function.generateRandomPrimeBigIntegerBiggerThan(e, rnd);

		// N = p.q
		BigInteger N = p.multiply(q);

		// calculate decryption exponent , d
		BigInteger pm1 = p.subtract(BigInteger.ONE);
		BigInteger qm1 = q.subtract(BigInteger.ONE);
		BigInteger d = e.modInverse(pm1.multiply(qm1));

		// set public key (N, e)
		publicKey.setE(e);
		publicKey.setN(N);
		publicKey.setHolderName(keyHolderName);

		// set private key (N, d)
		privateKey.setD(d);
		privateKey.setN(N);
	}

	/**
	 * encrypt - encrypt one block at a time using publicKey
	 * 
	 * @param plaintext - String
	 * @param publicKey - RsaPublicKey
	 * @return ciphertext - String
	 * 
	 */
	public String encrypt(String plaintext, RsaPublicKey publicKey) {
		// PLAIN_BLOCK_SIZE_BITS = 64;
		BigInteger[] Pblock = ConversionUtil.textToBigIntegerArr(plaintext, PLAIN_BLOCK_SIZE_BITS);
		BigInteger[] Cblock = new BigInteger[Pblock.length];
		for (int i = 0; i < Pblock.length; i++)
			Cblock[i] = EncryptOneBlock(Pblock[i], publicKey);
		return ConversionUtil.bigIntegerArrToHexStr(Cblock, CIPHER_BLOCK_SIZE_BITS);
	}

	/**
	 * EncryptOneBlock -
	 * 
	 * @param Pblock    - BigInteger
	 * @param publicKey - RsaPublicKey
	 * @return Cblock - BigInteger
	 * 
	 */
	public BigInteger EncryptOneBlock(BigInteger Pblock, RsaPublicKey publicKey) {
		BigInteger e = publicKey.getE();
		BigInteger N = publicKey.getN();
		return Pblock.modPow(e, N);
	}

	/**
	 * decrypt - decrypt ciphertext blocks one at a time
	 * 
	 * @param ciphertext - String
	 * @param privateKey - RsaPrivateKey
	 * @return plaintext - String
	 * 
	 */
	public String decrypt(String ciphertext, RsaPrivateKey privateKey) {
		// CIPHER_BLOCK_SIZE_BITS = 128;
		BigInteger[] Cblock = ConversionUtil.hexStrToBigIntegerArr(ciphertext, CIPHER_BLOCK_SIZE_BITS);
		BigInteger[] Pblock = new BigInteger[Cblock.length];
		for (int i = 0; i < Cblock.length; i++)
			Pblock[i] = DecryptOneBlock(Cblock[i], privateKey);
		return ConversionUtil.bigIntegerArrToText(Pblock, PLAIN_BLOCK_SIZE_BITS).trim();
	}

	/**
	 * DecryptOneBlock -
	 * 
	 * @param Cblock     - BigInteger
	 * @param privateKey - RsaPrivateKey
	 * @return Pblock - BigInteger
	 * 
	 */
	public BigInteger DecryptOneBlock(BigInteger Cblock, RsaPrivateKey privateKey) {
		BigInteger d = privateKey.getD();
		BigInteger N = privateKey.getN();
		return Cblock.modPow(d, N);
	}
}
