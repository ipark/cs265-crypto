package edu.sjsu.crypto.ciphersys.stream;


import org.junit.jupiter.api.Test;
import lombok.extern.slf4j.Slf4j;

@Slf4j
class A5_1SysTest {

	@Test
	void testEncryptionDemo() {
//		String plaintext = "defend the east wall!"; 
//		String pass = "GoToHell#007";

//		String plaintext = "The Blowfish Encryption Algorithm";
//		String pass = "Dr.Dobb'sJournal";

		String plaintext = "3rd Assignment Completed!!!";
		String pass = "Avengers#18-1/4";
		
		A5_1Sys sys = new A5_1Sys(pass);
		log.info("ciphertext = [" + sys.encrypt(plaintext)+ "]");
	}
	
	@Test
	void testDecryptionDemo() {
//		String ciphertext = "1df58522801a1789fa5c9d48486efedbc3c39acc05"; 
//		String pass = "GoToHell#007";
		
//		String ciphertext ="2c3907ee5f9a2f448065dcbd39e7578f41e6e4d6fe65332fbab69bf12eb114ab22";
//		String pass = "Dr.Dobb'sJournal";
		
		String ciphertext = "b51aa6b0dd663e30a23ab7765455c1e054a6d10c545297a19f9cab";
		String pass = "Avengers#18-1/4";
		
		A5_1Sys sys = new A5_1Sys(pass);
		log.info("Recovered Plaintext = [" + sys.decrypt(ciphertext)+ "]");
	}
}

