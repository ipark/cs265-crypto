package edu.sjsu.crypto.ciphersys.stream;


import edu.sjsu.yazdankhah.crypto.util.abstracts.A5_1Abs;
import edu.sjsu.yazdankhah.crypto.util.cipherutils.ConversionUtil;
import edu.sjsu.yazdankhah.crypto.util.cipherutils.Function;
import edu.sjsu.yazdankhah.crypto.util.cipherutils.StringUtil;
import edu.sjsu.yazdankhah.crypto.util.primitivedatatypes.Bit;
import edu.sjsu.yazdankhah.crypto.util.shiftregisters.LFSR;
//import lombok.extern.slf4j.Slf4j;

/**
 * @author Inhee Park
 * 
 * CS265 Programming Assignment #3 : A5/1
 * 
 * My customer name is Tracy Ho
 * 
 * ciphertext = [2c3907ee5f9a2f448065dcbd39e7578f41e6e4d6fe65332fbab69bf12eb114ab22]
 * pass = "Dr.Dobb'sJournal"
 * 
 * plaintext = [The Blowfish Encryption Algorithm]
 * 
 */


public class A5_1Sys extends A5_1Abs {
	// attributes
	LFSR X, Y, Z; 
	
	// constructor
	public A5_1Sys(String pass) {   	
		// pass: text -> binary string
		String keyBinStr = ConversionUtil.textToBinStr(pass);
		// extract 64-bit from binary string
		keyBinStr = StringUtil.rightTruncRightPadWithZeros(keyBinStr, 64); // 64-bit
		
		// 19-bit binary string for X-register
		String xbin = keyBinStr.substring(0,  18+1); // 18-0+1=19
		// 22-bit binary string for Y-register
		String ybin = keyBinStr.substring(19, 40+1); // 40-19+1=22
		// 23-bit binary string for Z-register
		String zbin = keyBinStr.substring(41, 63+1); // 63-41+1=23 
		
		int[] xtabs = {13, 16, 17, 18};
		int[] ytabs = {20, 21};
		int[] ztabs = {7, 20, 21, 22};
		
		// construct linear feedback shift register (LFSR)
		X = LFSR.constructFromBinStr(xbin, xtabs);
		Y = LFSR.constructFromBinStr(ybin, ytabs);
		Z = LFSR.constructFromBinStr(zbin, ztabs);
    }

	/**
	 * generateKey
	 * Generates key stream one bit at a time.
	 * 
	 * @param
	 * @return key
	 */
	public Bit generateKey() {
		Bit Gx, Gy, Gz, X8, Y10, Z10, key, m;
		
		// initialization Bit zero
		Gx = Bit.zero();
		Gy = Bit.zero();
		Gz = Bit.zero();
		
		// hard-coded bits
		X8  = X.bitAt(8);
		Y10 = Y.bitAt(10);
		Z10 = Z.bitAt(10);
		
		Bit[] chkBits = {X8, Y10, Z10};
		m = Function.maj(chkBits); // 1 if 1's are major; 0 otherwise
		
		if (m.equal(X8)) { // m == X8
			Gx = X.stepM(); // X must be mutated at every step
		}
		if (m.equal(Y10)) { // m == Y10
			Gy = Y.stepM(); // Y must be mutated at every step
		}
		if (m.equal(Z10)) { // m == Z10
			Gz = Z.stepM(); // Y must be mutated at every step
		}
		key = Gx.xor(Gy).xor(Gz); // Ki = Gx(xor)Gy(xor)Gz
		return key; // Bit (1-bit at a time)
	}
	
	/** 
	 * encrypt
	 * 
	 * @param plaintext in String format
	 * @return ciphertext in HEX format
	 * 
	 */	
	public String encrypt(String plaintext) {
		// text -> binary string
		String plainBinStr = ConversionUtil.textToBinStr(plaintext);
		// binary string -> bit array
		Bit[] plainBitArr = ConversionUtil.binStrToBitArr(plainBinStr);
		// store encrypted bit to bit array 
		Bit[] cipherBitArr = new Bit[plainBitArr.length];
		Bit Pi, Ki, Ci;
		// encrypt one bit at a time from generateKey()
		for (int i = 0; i < plainBitArr.length; i++) {
			Pi = plainBitArr[i];
			Ki = generateKey();
			Ci = Pi.xor(Ki); // Ci = Pi(xor)Ki
			cipherBitArr[i] = Ci;
		}
		// bit array -> binary string
		String binStr = ConversionUtil.bitArrToBinStr(cipherBitArr);
		// binary string -> HEX format
		String ciphertext = ConversionUtil.binStrToHexStr(binStr);
		return ciphertext;
	}
	/** 
	 * decrypt
	 * 
	 * @return ciphertext in HEX format
	 * @param plaintext in String format
	 * 
	 */	
	public String decrypt(String ciphertext) {	
		// HEX format -> binary string
		String cipherBinStr = ConversionUtil.hexStrToBinStr(ciphertext);
		// binary string -> binary bit array
		Bit[] cipherBitArr = ConversionUtil.binStrToBitArr(cipherBinStr);
		// store decrypted bit to bit array
		Bit[] plainBitArr = new Bit[cipherBitArr.length];
		Bit Pi, Ki, Ci;
		// decrypt one bit at a time from generateKey()
		for (int i = 0; i < cipherBitArr.length; i++) {
			Ci = cipherBitArr[i];
			Ki = generateKey();
			Pi = Ci.xor(Ki); // Pi = Ci(xor)Ki
			plainBitArr[i] = Pi;
		}
		// binary bit array ->  String format
		String plaintext = ConversionUtil.bitArrToText(plainBitArr);
		return plaintext;
	}

}
	
	
