package edu.sjsu.crypto.ciphersys.publicKey;

import org.junit.jupiter.api.Test;
import lombok.extern.slf4j.Slf4j;
import java.io.File;

@Slf4j
class KnapsackTest {

	String path = new File(".").getAbsolutePath();

/*	
	@Test
	void testGenerateKeyDemo() {
		String pass = "Shleter-in-place";
		String keyHolderName = "InheePark";

		String publicKeyFile = path + "/src/test/resources/IPknapsackPublicKey.txt";
		String privateKeyFile = path + "/src/test/resources/IPknapsackPrivateKey.txt";

		KnapsackSys sys = new KnapsackSys();
		sys.generateKeys(pass, keyHolderName);
		sys.getPrivateKey().save(privateKeyFile);
		sys.getPublicKey().save(publicKeyFile);

		log.info("Private Key = [\n" + sys.getPrivateKey() + "\n]\n");
		log.info("Public Key = [\n" + sys.getPublicKey() + "\n]\n");
	}
*/
	@Test
	void testEncryptionDemo() {
		String plaintext = "defend the east wall!";
		String publicKeyFile = path + "/src/test/resources/knapsackPublicKey.txt";

//		String plaintext = "See You Again Soon!";
//      String publicKeyFile = path + "/src/test/resources/IPknapsackPublicKey.txt";
//      String publicKeyFile = path + "/src/test/resources/Raki_knapsackPublicKey.txt";
		KnapsackSys sys = new KnapsackSys();
		sys.getPublicKey().restore(publicKeyFile);
		log.info("ciphertext = [" + sys.encrypt(plaintext, sys.getPublicKey()) + "]");
	}

	@Test
	void testDecryptionDemo() {
		String ciphertext = "2ee0aad3959068d7721e66a89afc76c7702445df2a22e5347a527ad899d0025cdbb3362f103dde8ad569e122b78963267ffde6";
		String privateKeyFile = path + "/src/test/resources/knapsackPrivateKey.txt";

//		String ciphertext = "238a33a18fc3c44042f21b2601029c450d2b105a8861718f7f431ae4f9a5b77fcbba2a7c3b9835264370b";
//		String ciphertext = "2d1d6e1162a0bed7b2bd1a31983d714d512cde99418d2bba7972b1718dd195257b17326730f4d5c16b2cb3869b9e3ee11c829f28e65ba753c1abc0c15e1a700e8785537b";
//		String privateKeyFile = path + "/src/test/resources/IPknapsackPrivateKey.txt";
		KnapsackSys sys = new KnapsackSys();
		sys.getPrivateKey().restore(privateKeyFile);
		log.info("PlaintextR = [" + sys.decrypt(ciphertext, sys.getPrivateKey()) + "]");
	}
}
