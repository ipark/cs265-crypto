package edu.sjsu.crypto.ciphersys.publicKey;

import edu.sjsu.yazdankhah.crypto.util.abstracts.KnapsackAbs;
import edu.sjsu.yazdankhah.crypto.util.ciphersysdatatypes.Knapsack;
import edu.sjsu.yazdankhah.crypto.util.ciphersysdatatypes.KnapsackPrivateKey;
import edu.sjsu.yazdankhah.crypto.util.ciphersysdatatypes.KnapsackPublicKey;
import edu.sjsu.yazdankhah.crypto.util.cipherutils.ConversionUtil;
import edu.sjsu.yazdankhah.crypto.util.cipherutils.Function;
import edu.sjsu.yazdankhah.crypto.util.cipherutils.StringUtil;
import edu.sjsu.yazdankhah.crypto.util.primitivedatatypes.Bit;
import edu.sjsu.yazdankhah.crypto.util.primitivedatatypes.Word;
import lombok.Data;
import java.math.BigInteger;
import java.util.Random;

/**
 * @author Inhee Park 
 * 		
 *         CS265 - HW8 - Knapsack Cipher System
 * 
 *         My customers are Rakesh and Tracy
 * 
 *         pass=[Shleter-in-place]
 *         plaintext=[See You Again Soon!]
 *
 */
@Data
public class KnapsackSys extends KnapsackAbs {
	// attributes
	private KnapsackPrivateKey privateKey;
	private KnapsackPublicKey publicKey;

	// constructor
	public KnapsackSys() {
		/*** CRUCIAL to avoid NullPointException ***/
		privateKey = new KnapsackPrivateKey(null, null, null);
		publicKey = new KnapsackPublicKey(null, null);
	}

	/**
	 * generateKeys - generate and save public-key and private-key files using
	 * random generator using pass, also random generators or multiplicative m, and
	 * modulo p
	 * 
	 * @param pass
	 * @param keyHolderName
	 * 
	 */
	public void generateKeys(String pass, String keyHolderName) {

		pass = StringUtil.rightTruncRightPadWithZeros(pass, KEY_SIZE_BITS);

		// 1. generate super-increasing knapsack w
		Random rnd = Function.getRandomGenerator64(pass);
		Knapsack w = new Knapsack(KNAPSACK_SIZE, rnd);

		// 2. convert w to regular knapsack w' w/ m and p
		BigInteger sum = w.sum();
		BigInteger m = Function.generateRandomPositiveInteger(rnd);
		BigInteger p = Function.generateRandomPrimeBigIntegerBiggerThan(sum, rnd);

		// 3. public key w'
		publicKey.setWp(w.toRegularKnapsack(m, p));
		publicKey.setHolderName(keyHolderName);

		// 4. private key (w, m, p)
		privateKey.setW(w);
		privateKey.setM(m);
		privateKey.setP(p);
	}

	/**
	 * encrypt - encrypt one block at a time using publicKey
	 * 
	 * @param plaintext
	 * @param publicKey
	 * @return ciphertext
	 * 
	 */
	public String encrypt(String plaintext, KnapsackPublicKey publicKey) {
		Word[] Pblock = ConversionUtil.textToWordArr(plaintext);
		BigInteger[] Cblock = new BigInteger[Pblock.length];
		for (int i = 0; i < Pblock.length; i++)
			Cblock[i] = EncryptOneBlock(Pblock[i], publicKey.getWp());
		return ConversionUtil.bigIntegerArrToHexStr(Cblock, CIPHER_BLOCK_SIZE_BITS);
	}

	/**
	 * EncryptOneBlock - calculate CSum = sum of w' elements based on the m={0,1}
	 * 
	 * @param Ptextblock
	 * @param publicKey.W'
	 * @return CSum
	 * 
	 */
	public BigInteger EncryptOneBlock(Word Pblock, Knapsack Wp) {
		Bit[] m = Pblock.toBitArr();
		assert m.length == Wp.getSize();
		BigInteger CSum = BigInteger.ZERO;
		for (int i = 0; i < m.length; i++) {
			if (m[i].isOne())
				CSum = CSum.add(Wp.memberAt(i));
		}
		return CSum;
	}

	/**
	 * decrypt - decrypt ciphertext blocks one at a time
	 * 
	 * @param ciphertext
	 * @param privateKey
	 * @return plaintext
	 * 
	 */
	public String decrypt(String ciphertext, KnapsackPrivateKey privateKey) {
		BigInteger[] Cblock = ConversionUtil.hexStrToBigIntegerArr(ciphertext, CIPHER_BLOCK_SIZE_BITS);
		Word[] Pblock = new Word[Cblock.length];
		for (int i = 0; i < Cblock.length; i++)
			Pblock[i] = DecryptOneBlock(Cblock[i], privateKey.getW(), privateKey.getM(), privateKey.getP());
		return ConversionUtil.wordArrToText(Pblock).trim();
	}

	/**
	 * DecryptOneBlock - solving the super-increasing-knapsack using private-key and
	 * cipherblock * inverse multiplicative of mod p
	 * 
	 * @param privateKey.W
	 * @param privateKey.m
	 * @param privateKey.p
	 * @return encrypted word
	 */
	public Word DecryptOneBlock(BigInteger Cblock, Knapsack W, BigInteger m, BigInteger p) {
		// Cp = (C x m^-1) % p
		BigInteger mInv = m.modInverse(p); // m^-1
		BigInteger Cp = Cblock.multiply(mInv).mod(p);
		Bit[] X = solveSuperIncreasingKnapsack(W, Cp);
		return Word.constructFromBitArr(X);
	}

	/**
	 * solveSuperIncreasingKanpsack - from privatekey.W and target sum, solving
	 * knapsack problem to obtain binary bits
	 * 
	 * @param privateKey.W
	 * @param sum
	 * @return solution of super-increasing-knapsack
	 * 
	 */
	public Bit[] solveSuperIncreasingKnapsack(Knapsack W, BigInteger sum) {
		Bit[] X = new Bit[W.getSize()];
		BigInteger targetSum = sum;
		for (int i = W.getSize() - 1; i >= 0; i--) {
			if (W.memberAt(i).compareTo(targetSum) <= 0) { // w <= targetSum
				X[i] = Bit.one();
				targetSum = targetSum.subtract(W.memberAt(i));} 
			else X[i] = Bit.zero(); // w > targetSum
		}
		return X;
	}

}
