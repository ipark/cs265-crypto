package edu.sjsu.crypto.ciphersys.block;


import org.junit.jupiter.api.Test;
import lombok.extern.slf4j.Slf4j;

@Slf4j
class CmeaTest {

	@Test
	void testEncryptionDemo() {
//		String plaintext = "defend the east wall!"; 
//		String pass = "GoToHell#007";
//		String plaintext = "Hello, World";
//		String pass = "DuncanHall6:55PM";
		String plaintext = "##### Deep Reinforcement Learning #####";
		String pass = "DuncanHall6:55PM";
		CmeaSys sys = new CmeaSys(pass);
		log.info("ciphertext = [" + sys.encrypt(plaintext)+ "]");
	}
	
	@Test
	void testDecryptionDemo() {
//		String ciphertext = "8747a983679b47dbab8fd1990860edfe588056f834eb09cf"; 
//		String pass = "GoToHell#007";
//		String ciphertext = "1d603ea59238a8abba60a2b0";
//		String pass = "DuncanHall6:55PM";
//		String ciphertext = "1d55d8ce13cb1401a39d049e16bdaab222e673676645155b";
//		String pass = "#007";
//		String ciphertext = "c7b5c6f48baeb3a5c78384d3c4fb915af047189a9eca3653";
//		String pass = "DuncanHall6:55PM";
		String ciphertext="f6ed5dc84af834623424a9f2ceb5519322eb444ecca109fc914ce36935ddd5b3e2ca442cf2630537";
		String pass = "DuncanHall6:55PM";
//		String ciphertext = "6947282fbf161e9df377d6b6bab875c93186998a1f8d66fef0c87ee32983d3e817357444d9db25cde0475e6312a1cf45df24086a246659d255673bded454c1b44967ef896b96b35a375756482c398dbc4eb6b5414da27bc604492dd0a800e09db3a1aae2ad3e329696ad4ba567e17308";
//		String pass = "CaroleTuesday#997";
		CmeaSys sysR = new CmeaSys(pass);
		log.info("Recovered Plaintext = [" + sysR.decrypt(ciphertext)+ "]");
	}
}

