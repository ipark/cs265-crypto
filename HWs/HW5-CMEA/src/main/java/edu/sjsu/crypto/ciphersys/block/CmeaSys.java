package edu.sjsu.crypto.ciphersys.block;

import edu.sjsu.yazdankhah.crypto.util.abstracts.CmeaAbs;
import edu.sjsu.yazdankhah.crypto.util.cipherutils.ConversionUtil;
import edu.sjsu.yazdankhah.crypto.util.cipherutils.StringUtil;
import edu.sjsu.yazdankhah.crypto.util.primitivedatatypes.Word;
import edu.sjsu.yazdankhah.crypto.util.primitivedatatypes.UByte;


/**
 * @author Inhee Park
 * 
 * CS265 Programming Assignment #5 : CMEA
 * 
 * My customers are <Tracy Ho> and <Rakesh Nagaraju>. 
 * 
 * ciphertext=[f6ed5dc84af834623424a9f2ceb5519322eb444ecca109fc914ce36935ddd5b3e2ca442cf2630537]
 * pass = "DuncanHall6:55PM"
 *  
 * Recovered Plaintext = [##### Deep Reinforcement Learning #####]
 * 
 */
public class CmeaSys extends CmeaAbs {
	// attributes
	private UByte[] K = new UByte[8];
	
	// constructor
	public CmeaSys(String pass) {   	
		
		// truncate 64-bit text -> binary -> hex array
		// this works for long keys
		//
		//String t64 = StringUtil.rightTruncRightPadWithZeros(pass, KEY_SIZE_BITS); 
		//String binStr = ConversionUtil.textToHexStr(t64);
		//K = ConversionUtil.hexStrToUByteArr(binStr); // K[0],...,K[7]
		
		// text -> binary -> make it 64-bit -> hex array
		// this doesn't work for long keys
		//
		String binStr = ConversionUtil.textToBinStr(pass);
		binStr = StringUtil.rightTruncRightPadWithZeros(binStr, KEY_SIZE_BITS); 
		K = ConversionUtil.binStrToUByteArr(binStr); // K[0],...,K[7]
    }

	/** 
	 * encrypt
	 * 
	 * @param plaintext in String format
	 * @return ciphertext in HEX format
	 * 
	 */	
	public String encrypt(String plaintext) {
		// text -> WordArr
		Word[] Pblocks = ConversionUtil.textToWordArr(plaintext);

		// store encrypted blocks
		Word[] Cblocks = new Word[Pblocks.length];
		
		/////////// one block at a time ////////
		for (int i = 0; i <= Pblocks.length - 1; ++i) {
			Cblocks[i] = EncryptOneBlock(Pblocks[i]);
		}
		/////////// one block at a time ////////
		
		// WordArr -> HEX str
		String ciphertext = ConversionUtil.wordArrToHexStr(Cblocks);
		return ciphertext;

	}
	
	/** 
	 * decrypt
	 * 
	 * @param ciphertext in HEX format
	 * @return plaintext in String format
	 * 
	 */	
	public String decrypt(String ciphertext) {
		// HEX str -> WordArr
		Word[] Cblocks = ConversionUtil.hexStrToWordArr(ciphertext);

		// store decrypted blocks
		Word[] Pblocks = new Word[Cblocks.length];

		/////////// one block at a time ////////
		for (int i = 0; i <= Cblocks.length - 1; ++i) {
			Pblocks[i] = EncryptOneBlock(Cblocks[i]);
		}
		/////////// one block at a time ////////
		
		// UByte array ->  String format
		String plaintext = ConversionUtil.wordArrToText(Pblocks);
		return plaintext.trim();
	}
	
	/** 
	 * EncryptOneBlock
	 * 
	 * @param Word size block
	 * 
	 */	
	private Word EncryptOneBlock(Word Wblock) {
		UByte[] c;
		UByte[] pM = Wblock.toUByteArrM(); 
		Round_1(pM);
		Round_2(pM);
		c = Round_3(pM);	
		return Word.constructFromUByteArr(c);
	}
	
	/** 
	 * Round_1
	 * 
	 * @param Word size block
	 * 
	 */	
	private void Round_1(UByte[] pM) {
		UByte z = UByte.ZERO(); // z = 0
		int n = pM.length;
		for (int i = 0; i <= n - 1; ++i) {
			
			UByte ubyte_i = UByte.constructFromInteger(i);
			
			// pM[i] = pM[i] + T( z(xor)i )  (mod 256)
			pM[i] = pM[i].addMod256( T(z.xor(ubyte_i)) );
			
			// z = z + pM[i] (mod 256)
			z = z.addMod256M(pM[i]);
		}
	}
	
	/** 
	 * Round_2
	 * 
	 * @param Word size block
	 * 
	 */	
	private void Round_2(UByte[] pM) {
		int n = pM.length;
		int h = Math.floorDiv(n, 2);
		
		for (int i = 0; i <= h - 1; ++i) {
			// t = pM[n-1-i] V 1
			UByte t = pM[n-1-i].or(UByte.ONE());
			
			// pM[i] = pM[i] (xor) t
			//pM[i].xorM(t);
			pM[i] = pM[i].xorM(t);
		}
	}
	
	/** 
	 * Round_3
	 * 
	 * @param Word size block
	 * @return c - UByte array
	 * 
	 */	
	private UByte[] Round_3(UByte[] pM) {
		UByte z = UByte.ZERO(); // z = 0
		UByte[] c = new UByte[pM.length]; // c[]
		int n = pM.length;
		
		for (int i = 0; i <= n - 1; ++i) {
			UByte ubyte_i = UByte.constructFromInteger(i);
			
			// k = T( z(xor)i )
			UByte k = T( z.xor(ubyte_i) );
			
			// z = z + pM[i] (mod 256)
			z = z.addMod256(pM[i]);
			
			// c[i] = pM[i] - k (mod 256)
			c[i] = pM[i].subtractMod256(k);
		}
		return c;
	}
	
	/** 
	 * Function T
	 * 
	 * @param Word size block
	 * @return F UByte
	 * 
	 */	
	private UByte T(UByte x) {
		//        ( x(xor)K[0]  )          + K[1] (mod 256)
		UByte q = ( x.xor(K[0]) ).addMod256( K[1] );
		//    Q(x) =               C[q] +          x (mod 256)
		UByte Q = CAVE_LOOKUP.lookUp(q).addMod256(x);
		
		//        ( Q(xor)K[2]  )          + K[3] (mode 256)
		UByte r = ( Q.xor(K[2]) ).addMod256M( K[3] );
		//    R(x) =               C[r]          + x (mod 256)
		UByte R = CAVE_LOOKUP.lookUp(r).addMod256(x);
		
		//        ( R(xor)K[4]  )          + K[5] (mode 256)
		UByte s = ( R.xor(K[4]) ).addMod256M( K[5] );
		//    S(x) =               C[s]          + x (mod 256)
		UByte S = CAVE_LOOKUP.lookUp(s).addMod256(x);
		
		//        ( S(xor)K[6]  )          + K[7] (mode 256)
		UByte f = ( S.xor(K[6]) ).addMod256M( K[7] );
		//    F(x) =               C[f]          + x (mod 256)
		UByte F = CAVE_LOOKUP.lookUp(f).addMod256(x);
		return F;
	}
}
	
	
