package edu.sjsu.crypto.ciphersys.block;

import edu.sjsu.yazdankhah.crypto.util.abstracts.TeaAbs;
import edu.sjsu.yazdankhah.crypto.util.cipherutils.ConversionUtil;
import edu.sjsu.yazdankhah.crypto.util.cipherutils.StringUtil;
import edu.sjsu.yazdankhah.crypto.util.primitivedatatypes.Word;
import edu.sjsu.yazdankhah.crypto.util.primitivedatatypes.DWord;

/**
 * @author Inhee Park
 * 
 * CS265 Programming Assignment #6 : TEA
 * 
 * My customers are <Tracy Ho> and <Rakesh Nagaraju>.
 * 
 * ciphertext = [61877fcc0a19b5bd53e6812281576384298ebf5903c2df906cebb9e5b0b08d11]
 * pass = "COVID-19"
 * 
 * 
 * Recovered Plaintext = [coding challenge information]
 * 
 */
public class TeaSys extends TeaAbs {
	// attributes
	private Word[] SK = new Word[4];
	
	// constructor
	public TeaSys(String pass) {
		String binStr = ConversionUtil.textToBinStr(pass);
		binStr = StringUtil.rightTruncRightPadWithZeros(binStr, KEY_SIZE_BITS);
		SK = ConversionUtil.binStrToWordArr(binStr); // SK[0],...,SK[3]
	}

	/**
	 * encrypt - 
	 * Take plaintext as input, 
	 * convert it to DWord array (64-bit block size),
	 * apply EncryptOneBlock with one block at a time from Pblocks, 
	 * store the encrypted block to Cblocks, 
	 * finally return Cblocks to hex string
	 * 
	 * @param plaintext in String format
	 * @return ciphertext in HEX format
	 * 
	 */
	public String encrypt(String plaintext) {
		DWord[] Pblocks = ConversionUtil.textToDWordArr(plaintext);
		DWord[] Cblocks = new DWord[Pblocks.length];
		for (int i = 0; i < Pblocks.length; ++i) 
			Cblocks[i] = EncryptOneBlock(Pblocks[i]);
		String ciphertext = ConversionUtil.dwordArrToHexStr(Cblocks);
		return ciphertext;
	}

	/**
	 * decrypt - 
	 * Take ciphertex in hex format as input, 
	 * convert it to DWord array (64-bit block size),
	 * apply DecryptOneBlock with one block at a time from Cblocks, 
	 * store the decrypted block to Pblocks, 
	 * finally return Pblocks to string
	 * 
	 * @param ciphertext in HEX format
	 * @return plaintext in String format
	 * 
	 */
	public String decrypt(String ciphertext) {
		DWord[] Cblocks = ConversionUtil.hexStrToDWordArr(ciphertext);
		DWord[] Pblocks = new DWord[Cblocks.length];
		for (int i = 0; i < Cblocks.length; ++i) 
			Pblocks[i] = DecryptOneBlock(Cblocks[i]);
		String plaintext = ConversionUtil.dwordArrToText(Pblocks);
		return plaintext.trim();
	}

	/**
	 * EncryptOneBlock -
	 * from the double word plaintext block,
	 * split into left word and right word.
	 * Go through Feistel Operations with L_{r-1} and R_{r-1},
	 * resulting L_r and R_r are passed to the next round
	 * repeat such operations 32 rounds
	 * 
	 * @param DWord size block
	 * @return DWord encrypted block (L,R)
	 * 
	 */
	private DWord EncryptOneBlock(DWord P) {
		Word L = P.leftWord();
		Word R = P.rightWord();
		Word sum = Word.ZERO();
		
		for (int r = 1; r <= ROUNDS; ++r) {
			sum.addMod2p32M(DELTA_WORD);
			//
			Word L1 = R.shiftLeft(4).addMod2p32(SK[0]);
			Word L2 = R.addMod2p32(sum);
			Word L3 = R.shiftRight(5).addMod2p32(SK[1]);
			L = L.addMod2p32(L1.xor(L2).xor(L3));
			//
			Word R1 = L.shiftLeft(4).addMod2p32(SK[2]);
			Word R2 = L.addMod2p32(sum);
			Word R3 = L.shiftRight(5).addMod2p32(SK[3]);
			R = R.addMod2p32(R1.xor(R2).xor(R3));
		}
		return DWord.constructFrom2Words(L, R);
	}

	/**
	 * DecryptOneBlock -
	 * from the double word ciphertext block,
	 * split into left word and right word.
	 * Go through Feistel Operations with L_{r-1} and R_{r-1},
	 * resulting L_r and R_r are passed to the next round
	 * repeat such operations 32 rounds
	 * 
	 * @param DWord size block
	 * @return DWord decrypted block (L,R)
	 * 
	 */
	private DWord DecryptOneBlock(DWord C) {
		Word L = C.leftWord();
		Word R = C.rightWord();
		Word sum = DELTA_WORD.shiftLeft(5);
		
		for (int r = 1; r <= ROUNDS; ++r) {
			//
			Word R1 = L.shiftLeft(4).addMod2p32(SK[2]);
			Word R2 = L.addMod2p32(sum);
			Word R3 = L.shiftRight(5).addMod2p32(SK[3]);
			R = R.subtractMod2p32(R1.xor(R2).xor(R3));
			//
			Word L1 = R.shiftLeft(4).addMod2p32(SK[0]);
			Word L2 = R.addMod2p32(sum);
			Word L3 = R.shiftRight(5).addMod2p32(SK[1]);
			L = L.subtractMod2p32(L1.xor(L2).xor(L3));
			//
			sum.subtractMod2p32M(DELTA_WORD);
		}
		return DWord.constructFrom2Words(L, R);
	}

	
}
