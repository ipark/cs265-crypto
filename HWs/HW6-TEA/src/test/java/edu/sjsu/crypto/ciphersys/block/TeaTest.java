package edu.sjsu.crypto.ciphersys.block;

import org.junit.jupiter.api.Test;
import lombok.extern.slf4j.Slf4j;

@Slf4j
class TeaTest {

	@Test
	void testEncryptionDemo() {
//		String plaintext = "defend the east wall!";
//		String pass = "GoToHell#007";
		
		String plaintext = "coding challenge information";
		String pass = "COVID-19";

				TeaSys sys = new TeaSys(pass);
		log.info("ciphertext = [" + sys.encrypt(plaintext) + "]");
	}

	@Test
	void testDecryptionDemo() {
//		String ciphertext = "b02283722e3e1e115231392306f46170dbf0a4c908b4f6e2";
//		String pass = "GoToHell#007";

//		String ciphertext = "61877fcc0a19b5bd53e6812281576384298ebf5903c2df906cebb9e5b0b08d11";
//		String pass = "COVID-19";
		
		String ciphertext = "6c2cc35ae1be383001aa6f5ac347893453f025a20ca19619c30c7a299829b5f8";
		String pass = "COVID-19";
		
		TeaSys sysR = new TeaSys(pass);
		log.info("Recovered Plaintext = [" + sysR.decrypt(ciphertext) + "]");
	}
}
