package edu.sjsu.crypto.ciphersys.block;

import edu.sjsu.yazdankhah.crypto.util.abstracts.AesAbs;
import edu.sjsu.yazdankhah.crypto.util.ciphersysdatatypes.AesState;
import edu.sjsu.yazdankhah.crypto.util.cipherutils.ConversionUtil;
import edu.sjsu.yazdankhah.crypto.util.cipherutils.StringUtil;
import edu.sjsu.yazdankhah.crypto.util.primitivedatatypes.*;

/**
 * @author Inhee Park
 * 
 * CS265 Midterm (3/27/2020)
 * AES-128 (Advanced Encryption Standard with 128-bit key size)
 * 
 */
public class AesSys extends AesAbs {
	// AesState is 4x4 UByte column-wise matrix data structured in AES
	private AesState[] SK = new AesState[SUBKEYS_ARRAY_SIZE]; // 11x128-bit subkeys
	// constructor
	public AesSys(String pass) {
		pass = ConversionUtil.textToBinStr(pass);
		pass = StringUtil.rightTruncRightPadWithZeros(pass, KEY_SIZE_BITS); // 128-bit
		AesState K = AesState.constructFromBinStr(pass);
		SK = GenerateSubKeys(K);
	}
	/**
	 * GenerateSubKeys - generate 11x128-bit sub-keys through r-round
	 * 
	 * @param K - input key in AesState to start with
	 * @return SK - generated 11 x 128-bit sub-keys in AesState[]
	 */
	private AesState[] GenerateSubKeys(AesState K) {
		SK[0] = K;
		for (int r = 1; r <= ROUNDS; r++) // 10 rounds
			SK[r] = OneRoundKeyGeneration(SK[r - 1], r);
		return SK;
	}
	
	/**
	 * OneRoundKeyGeneration - generate SK[r] from SK[r-1] and r
	 * 
	 * @param Q - SK[r-1] in AesState
	 * @param r - current round in int
	 * @return SK[r] in AesState
	 */
	private AesState OneRoundKeyGeneration(AesState Q, int r) {
		// Q = SK[r-1]
		Word[] WArr = Q.toWordArr(); // WArr[0]..WArr[3]
		WArr[0].xorM( g(WArr[3], r) ); // g-function
		for (int i = 1; i < WArr.length; i++) 
			WArr[i].xorM(WArr[i - 1]);
		return AesState.constructFromWordArr(WArr); // SK[r]
	}
	
	/**
	 * g - UByte-level manipulation of a Word from SK[r-1]
	 * 
	 * @param W0 - input Word converted from SK[r-1] AesState
	 * @param r - current round in int
	 * @return W1 - Word output with UByte-level manipulation
	 */
	private Word g(Word W0, int r) {
		// W0: {V0, V1, V2, V3}
		// W1: {V1, V2, V3, V0}
		Word W1 = W0.rotateLeftBytes();
		// W1 = SBOX(W1)
		// 1) split Word into UByte[] V
		// 2) per UByte V[i]<-SBOX_lookUp(V[i])
		// 3) UByte operation: V[0] (xor) RC[r-1]
		// 4) convert V[] to Word
		UByte[] V = W1.toUByteArr();
		for (int i = 0; i < V.length; i++) 
			V[i] = SBOX.lookUp(V[i]);
		V[0].xorM(RC[r - 1]); // <W1>0..7 (xor) RC[r-1]
		W1 = Word.constructFromUByteArr(V);
		return W1;
	}
	
	/**
	 * encrypt 
	 * 
	 * @param plaintext in String format
	 * @return ciphertext in HEX format
	 */
	public String encrypt(String plaintext) {
		AesState[] blocks = ConversionUtil.textToAesStateArr(plaintext);
		for (int i = 0; i < blocks.length; i++) 
			blocks[i] = EncryptOneBlock(blocks[i]);
		return ConversionUtil.aesStateArrToHexStr(blocks);
	}

	/**
	 * EncryptOneBlock 
	 * 
	 * @param AesState block
	 * @return AesState encrypted block
	 */
	private AesState EncryptOneBlock(AesState P) {
		P.addRoundKeyM(SK[0]);
		for (int r = 1; r < ROUNDS; r++) { // 1..9
			P.byteSubM(SBOX); // substitution by lookup table
			P.shiftRowM();
			P.mixColumnM(MIX_COLUMN_CONSTANT_STATE);
			P.addRoundKeyM(SK[r]);
		}
		// round 10
		P.byteSubM(SBOX);
		P.shiftRowM();
		P.addRoundKeyM(SK[10]);
		return P;
	}
	
	/**
	 * decrypt 
	 * 
	 * @param ciphertext in HEX format
	 * @return plaintext in String format
	 */
	public String decrypt(String ciphertext) {
		AesState[] blocks = ConversionUtil.hexStrToAesStateArr(ciphertext);
		for (int i = 0; i < blocks.length; i++) 
			blocks[i] = DecryptOneBlock(blocks[i]);
		return ConversionUtil.aesStateArrToText(blocks).trim();
	}

	/**
	 * DecryptOneBlock 
	 * 
	 * @param AesSTate block
	 * @return AesState decrypted block 
	 * 
	 */
	private AesState DecryptOneBlock(AesState C) {
		C.addRoundKeyM(SK[10]);
		C.invShiftRowM();
		C.byteSubM(INVERSE_SBOX); // invByteSub
		for (int r = ROUNDS - 1; r > 0; r--) { // 9..1
			C.addRoundKeyM(SK[r]);
			C.mixColumnM(INV_MIX_COLUMN_CONSTANT_STATE);
			C.invShiftRowM();
			C.byteSubM(INVERSE_SBOX); // invByteSub
		}
		C.addRoundKeyM(SK[0]);
		return C;
	}
	
}
