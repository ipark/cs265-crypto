package edu.sjsu.crypto.ciphersys.block;

import org.junit.jupiter.api.Test;
import lombok.extern.slf4j.Slf4j;

@Slf4j
class AesTest {

	@Test
	void testEncryptionDemo() {
		String plaintext = "defend the east wall!"; 
		String pass = "GoToHell#007";
		AesSys sys = new AesSys(pass);
		log.info("ciphertext = [" + sys.encrypt(plaintext)+ "]");
	}

	@Test
	void testDecryptionDemo() {
		String ciphertext = "1090004b37b36a6cef050d05b93cf07c8ce99892ee216cedca169ae6c586655e";
		String pass = "GoToHell#007";
		AesSys sysR = new AesSys(pass);
		log.info("Recovered Plaintext = [" + sysR.decrypt(ciphertext)+ "]");	}
}
