package edu.sjsu.crypto.ciphersys.block;

// PI decimal point in hexadecimal
//must add jar path with BlowFishData.jar
import edu.sjsu.ipark.cs265.BlowFishData; 

import edu.sjsu.yazdankhah.crypto.util.cipherutils.ConversionUtil;
import edu.sjsu.yazdankhah.crypto.util.primitivedatatypes.Word;
import edu.sjsu.yazdankhah.crypto.util.primitivedatatypes.DWord;
import edu.sjsu.yazdankhah.crypto.util.primitivedatatypes.UByte;
import edu.sjsu.yazdankhah.crypto.util.cipherutils.StringUtil;

/**
 * 
 * @author Inhee Park
 * CS 265 Project - The Blowfish Block Cipher System
 *
 * Original algorithm was designed by Bruce Schneier.
 * Unpatented, simple, secure and fast.
 * Fixed 64-bit block size; 
 * Variable-length key (32 to 448-bit); 
 * 16-round Feistel network. 
 * 
 * Key should be big enough to withstand a brute force attack 
 * (at least 128-bit). 
 * Highly expensive key scheduling.
 * 
 */
public class BlowFishSys {
	// attributes
	private static final int NUM_SUBKEYS = 18;
	private static final int NUM_SBOXES = 4;
	private static final int NUM_ENTRIES = 256;
	
	// must add jar path with BlowFishData.jar
	BlowFishData PI0xdata = new BlowFishData();
	private String[] P0x = PI0xdata.getParray();
	private String[][] S0x = PI0xdata.getSbox();
	
	private Word[] P = new Word[NUM_SUBKEYS];
	private Word[][] Sbox = new Word[NUM_SBOXES][NUM_ENTRIES];
	
	// constructor
	public BlowFishSys(String pass) {

		// construct Word[] P
		for (int i = 0; i < NUM_SUBKEYS; i++)
			P[i] = Word.constructFromHexStr(P0x[i]);
		// construct Word[][] Sbox
		for (int j = 0; j < NUM_SBOXES; j++)
			for (int k = 0; k < NUM_ENTRIES; k++)
				Sbox[j][k] = Word.constructFromHexStr(S0x[j][k]);
		
		pass = ConversionUtil.textToBinStr(pass);
		pass = StringUtil.toDividableByNRightPadZero(pass, 8); 
		UByte[] SK = ConversionUtil.binStrToUByteArr(pass); // min=32-bit,max=448-bit
		GenerateSubKeys(SK);
	}

	/**
	 * GenerateSubKeys - 
	 * 
	 * Large Subkey Space (4168 bytes) for steep brute-force key attack
	 * Expensive Key Scheduling : 
	 * 1. load Pi 
	 * 2. assign Pi
	 * 3. key-dependent mix byte-by-byte pass cycle
	 * 4. preventive mix by zero-string encryption
	 * 
	 * @param SK - UByte array for password
	 */

	// generate subkeys
	private void GenerateSubKeys(UByte[] SK) {
		// XOR P[0] with 1st 32-bit key
		// XOR p[1] with 2nd 32-bit key
		// ...until XOR for entire P[0..17]
		for (int i = 0; i < NUM_SUBKEYS; i++) {
			
			Word KeyWord = Word.ZERO_WORD;
			UByte[] CollectBytes = new UByte[4];
			for (int j = 0; j < 4; j++)
				CollectBytes[j] = SK[ j % SK.length];
			KeyWord = Word.constructFromUByteArr(CollectBytes);
			P[i].xorM(KeyWord);
		}
		
		// encrypt all zero-string using P and Sbox
		// continue to do step3-4 for entire P-array[18] and Sbox[4][256]
		Word nullL = Word.ZERO_WORD;
		Word nullR = Word.ZERO_WORD;

		for (int i = 0; i < NUM_SUBKEYS; i += 2) {
			DWord IN = DWord.constructFrom2Words(nullL, nullR);
			DWord OUT = EncryptOneBlock(IN); // unique in BlowFish
			P[i] = OUT.leftWordM();
			P[i + 1] = OUT.rightWordM();
		}

		for (int j = 0; j < NUM_SBOXES; j++) {
			for (int k = 0; k < NUM_ENTRIES; k += 2) {
				DWord IN = DWord.constructFrom2Words(nullL, nullR);
				DWord OUT = EncryptOneBlock(IN); // unique in BlowFish
				Sbox[j][k] = OUT.leftWordM();
				Sbox[j][k + 1] = OUT.rightWordM();
			}
		}
	}

	/**
	 * F - 
	 * 1-way Non-reversible Round Function F 
	 * Complexity Layer for Security	 
	 * @param left 32-bit - Word
	 * @return 1-way function output - Word
	 * 
	 */
	// function F
	private Word F(Word x) {
		Word y;
		y = SboxLookup(0, x).addMod2p32(SboxLookup(1, x));
		y = y.xor(SboxLookup(2, x));
		y = y.addMod2p32(SboxLookup(3, x));
		return y;
	}

	/**
	 * Sboxlookup - 
	 * @param Sbox row index
	 * @param 32-bit subkey to be converted for Sbox column index
	 * @return 32-bit element of Sbox[row][col] 
	 */
	 private Word SboxLookup(int index, Word x) {
	 
		return Sbox[index][x.byteAt(index).toInteger()];
	}

	/**
	 * encrypt - 
	 * @param plaintext
	 * @return ciphertex
	 */
	public String encrypt(String plaintext) { // blockSize = 64-bit
		DWord[] Pblocks = ConversionUtil.textToDWordArr(plaintext);
		DWord[] Cblocks = new DWord[Pblocks.length];
		for (int i = 0; i < Pblocks.length; i++)
			Cblocks[i] = EncryptOneBlock(Pblocks[i]);
		String ciphertext = ConversionUtil.dwordArrToHexStr(Cblocks);
		return ciphertext;
	}

	/**
	 * EncryptOneBlock - 
	 * 16 rounds Feistel network and extra swapping with subkey Parray
	 * @param Pblock
	 * @return Cblock
	 */
	private DWord EncryptOneBlock(DWord Pblock) {
		Word L = Pblock.leftWord();
		Word R = Pblock.rightWord();

		for (int i = 0; i < NUM_SUBKEYS; i += 2) {
			if (i == 0) {
				L.xorM(P[0]);
				R.xorM(F(L).xor(P[1]));
			} else if (i == 16) {
				L.xorM(F(R).xor(P[16]));
				R.xorM(P[17]);
			} else {
				L.xorM(F(R).xor(P[i]));
				R.xorM(F(L).xor(P[i + 1]));
			}
		} 
		
		Word temp = L; L = R; R = temp; // swap R<->L
		return DWord.constructFrom2Words(L, R);
	}

	/**
	 * decrypt - 
	 * @param ciphertext
	 * @return plaintext
	 */
	public String decrypt(String ciphertext) {
		DWord[] Cblocks = ConversionUtil.hexStrToDWordArr(ciphertext);
		DWord[] Pblocks = new DWord[Cblocks.length];
		for (int i = 0; i < Cblocks.length; i++)
			Pblocks[i] = DecryptOneBlock(Cblocks[i]);
		String plaintext = ConversionUtil.dwordArrToText(Pblocks);
		return plaintext.trim();
	}

	/**
	 * DecryptOneBlock - 
	 * 16 rounds Feistel network and extra swapping with subkey Parray
	 * @param Pblock
	 * @return Cblock
	 */

	private DWord DecryptOneBlock(DWord Cblock) {
		Word L = Cblock.leftWord();
		Word R = Cblock.rightWord();

		for (int i = NUM_SUBKEYS - 1; i > 0; i -= 2) {

			if (i == 17) {
				L.xorM(P[17]);
				R.xorM(F(L).xor(P[16]));
			} else if (i == 1) {
				L.xorM(F(R).xor(P[1]));
				R.xorM(P[0]);
			} else {
				L.xorM(F(R).xor(P[i]));
				R.xorM(F(L).xor(P[i - 1]));
			}
		}		
		Word temp = L; L = R; R = temp; // swap R<->L
		return DWord.constructFrom2Words(L, R);
	}
	
}
