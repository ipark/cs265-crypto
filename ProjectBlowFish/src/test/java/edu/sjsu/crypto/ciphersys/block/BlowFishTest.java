package edu.sjsu.crypto.ciphersys.block;


import org.junit.jupiter.api.Test;
import lombok.extern.slf4j.Slf4j;

/**
 * @author Inhee Park
 * CS 265 Project - The Blowfish Block Cipher System
 * JUnit test
 */
@Slf4j
class BlowFishTest {

	@Test
	void testEncryptionDemo() {
		String plaintext = "defend the east wall!";
		String pass = "GoToHell#007";
		
		//String plaintext = "The software magazine Dr. Dobbs Journal is sponsoring $1000 contest for the best cryptanalysis of Blowfish received before April 1995.";
		//String pass = "#Stay@Home#From:03-10-2020#Now:05-01-2020";
		
		
		BlowFishSys sys = new BlowFishSys(pass);
		log.info("ciphertext = [" + sys.encrypt(plaintext) + "]");

	}

	@Test
	void testDecryptionDemo() {
		String ciphertext = "9ce75145aef8b53154b1295c8aa450895a73ad12dd6a918c";
		String pass = "GoToHell#007";

		//String ciphertext = "dfb1364d3a34c6d0eaae0b39080bd0b24717d4c141918f08008345fa1ec9d7f537cb444be80033bde4b169dcc7f49990d9167510a678c4c0ff2aaf9a1bb52b8fc1e5cbe33b25bc5248afdb8bdb8964953c6a35df9794c65b4c621f3f9bf7aedd4e26ccec80efa7c90f5e276c18c030ba6c66d21cd42b8d54b3351c9050788294626fedfe7af09682";
		//String pass = "#Stay@Home#From:03-10-2020#Now:05-01-2020";
			
		
		BlowFishSys sysR = new BlowFishSys(pass);
		log.info("Recovered Plaintext = [" + sysR.decrypt(ciphertext) + "]");
	}
	
}
