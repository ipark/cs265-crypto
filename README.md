CS265 : Cryptography and Computer Security
=========
<ul>
<li>
Classical ciphers [[HW1: SimpleSubSys, HW2: DoubleTransposition]](https://gitlab.com/ipark/cs265-crypto/-/tree/master/HWs/HW2-DoubleTransposition)
</li>
<li>
Stream ciphers: A5/1 [[HW3]](https://gitlab.com/ipark/cs265-crypto/-/tree/master/HWs/HW3-C51Cipher/src)
</li>

<li>
Stream ciphers: ORYX
</li>

<li>
Stream ciphers: PKZIP [[HW4]](https://gitlab.com/ipark/cs265-crypto/-/tree/master/HWs/HW4-PKZIP/src)
</li>

<li>
Stream ciphers: RC4
</li>

<li>
Block ciphers: Feistel model, CMEA [[HW5]](https://gitlab.com/ipark/cs265-crypto/-/tree/master/HWs/HW5-CMEA/src)
</li>

<li>
Block ciphers: FEAL
</li>

<li>
Block ciphers: TEA [[HW6]](https://gitlab.com/ipark/cs265-crypto/-/tree/master/HWs/HW6-TEA/src)
</li>

<li>
Block ciphers: DES
</li>

<li>
Block ciphers: AES  [[Midterm]](https://gitlab.com/ipark/cs265-crypto/-/tree/master/Midterm/src)
</li>

<li>
Public key ciphers: Knapsack  [[HW8]](https://gitlab.com/ipark/cs265-crypto/-/tree/master/HWs/HW8-Knapsack/src)
</li>

<li>
Public key ciphers: RSA [[HW9]](https://gitlab.com/ipark/cs265-crypto/-/tree/master/HWs/HW9-RSA/src)
</li>

<li>
Hashing: MD4 [[Final]](https://gitlab.com/ipark/cs265-crypto/-/tree/master/Final/src)
</li>

<li>
Hashing: SHA
</li>

CS265 Project: The BlowFish Block Cipher System
=========
<img src="https://gitlab.com/ipark/cs265-crypto/raw/master/bf-logo.png" width="600">
<img src="https://gitlab.com/ipark/cs265-crypto/raw/master/bf-end.png" width="600">


### Codes: [Java src code](https://gitlab.com/ipark/cs265-crypto/-/tree/master/ProjectBlowFish/src/)

### Report: [Report](https://gitlab.com/ipark/cs265-crypto/-/blob/master/ProjectBlowFish/Summary/)

### Presentation: [Slide](https://gitlab.com/ipark/cs265-crypto/-/tree/master/ProjectBlowFish/Presentation)
